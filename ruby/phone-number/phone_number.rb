class PhoneNumber
  AREA_CODE_LENGTH = 3
  US_COUNTRY_CODE  = "1"
  BLANK = "0" * 10

  attr_reader :number

  def initialize(phone_number)
    @number = sanitize(phone_number)
  end

  def area_code
    number.slice(0, AREA_CODE_LENGTH)
  end

  def to_s
    "(%s) %s-%s" % number.partition(%r|
      (?<=                       # look behind
        \A                       # beginning of string
        \d{#{AREA_CODE_LENGTH}}  # 3 digits
      )
      \d{3}                      # 3 digits (before the dash)
    |x) # x makes Ruby ignore whitespace and comments inside regexp
  end

  private

  def sanitize(us_phone_number)
    num = us_phone_number.gsub(/[\W_]/, "")

    return BLANK if num =~ /\D/
    return BLANK unless [10, 11].include?(num.length)
    if num.length == 11
      return num[1..-1] if num.start_with?(US_COUNTRY_CODE)
      return BLANK
    end

    num
  end
end
