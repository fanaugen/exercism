class ETL
  def self.transform(old_scoring)
    old_scoring.each_with_object({}) do |(score, letters), hsh|
      letters.each {|letter| hsh[letter.downcase] = score}
    end
  end
end
