class Series
  def initialize(str)
    @digits = str
  end

  def slices(length)
    raise ArgumentError if length > @digits.length

    (0..@digits.length - length).map do |start_index|
      @digits.slice(start_index, length)
    end

  end
end
