module Accumulate
  def accumulate(&proc)
    result = Array.new(self.size)
    i = 0

    while i < self.size do
      result[i] = proc.call(self[i])
      i += 1
    end

    result
  end
end

class Array
  include Accumulate
end

module BookKeeping; VERSION = 1; end
