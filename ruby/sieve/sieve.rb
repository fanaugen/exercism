class Sieve
  attr_reader :range, :prime_numbers

  def initialize(limit)
    @range = (2..limit).to_a
    @prime_numbers = []
  end

  def primes
    while !range.empty? do
      sieve!
    end
    prime_numbers
  end

  private

  def sieve!
    prime = range.shift
    prime_numbers << prime
    range.reject! {|n| n % prime == 0}
  end
end

module BookKeeping
  VERSION = 1
end
