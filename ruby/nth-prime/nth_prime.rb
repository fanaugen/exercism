class Prime
  class << self

    def nth(n)
      raise ArgumentError if n < 1
      generator.take(n).last
    end

    def generator
      @prime_generator ||= Enumerator.new do |yielder|
        # start with a set of known primes
        primes = [2, 3, 5, 7, 11, 13, 17]
        initial_enum = primes.to_enum
        primes.length.times { yielder << initial_enum.next }

        loop do
          n = primes.last + 2
          while primes.take_while {|p| p <= Math.sqrt(n)}.any? {|p| n % p == 0} do
            n += 2
          end
          yielder << n
          primes  << n
        end
      end
    end

  end
end

module BookKeeping
  VERSION = 1
end
