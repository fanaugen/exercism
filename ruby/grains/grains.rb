class Grains
  BOARD_SQUARES = (1..64).freeze

  def self.square(number)
    raise ArgumentError unless BOARD_SQUARES.member? number
    2 ** (number - 1)
  end

  def self.total
    BOARD_SQUARES.inject(0) {|total, n| total + square(n)}
  end
end

module BookKeeping
  VERSION = 1
end
