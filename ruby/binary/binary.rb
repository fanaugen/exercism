class Binary
  def self.to_decimal(binary)
    validate_format(binary)
    number = 0
    bits = binary.chars.reverse!.map! &:to_i

    bits.each_with_index do |bit, exponent|
      number += bit * (2 ** exponent)
    end

    number
  end

  def self.validate_format(str)
    str =~ /\A[01]+\z/ or raise ArgumentError, "#{str} is not a valid binary number"
  end
end

module BookKeeping; VERSION = 3; end
