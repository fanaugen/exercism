class Raindrops
  def self.convert(n)
    result = ''
    case n
      when n % 3 == 0 then result += 'Pling'
      when n % 5 == 0 then result += 'Plang'
      when n % 7 == 0 then result += 'Plong'
      else result = n.to_s
    end
    result
  end
end
module BookKeeping
  VERSION = 3
end
