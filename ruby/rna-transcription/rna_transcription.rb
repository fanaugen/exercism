class Complement

  DNA_CHARS = "CGTA".freeze
  RNA_CHARS = "GCAU".freeze
  VALID = Proc.new {|char| DNA_CHARS.include? char}

  def self.of_dna(dna_string)
    return "" unless dna_string.chars.all? &VALID

    dna_string.tr DNA_CHARS, RNA_CHARS
  end

end

module BookKeeping
  VERSION = 4
end
