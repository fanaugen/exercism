class Pangram
  ALPHABET = 'a'..'z'

  def self.pangram?(str)
    str = str.downcase
    ALPHABET.all? {|c| str.include? c}
  end
end

module BookKeeping
  VERSION = 3
end
