class Hamming
  def self.compute(strand_a, strand_b)
    assert_comparable!(strand_a, strand_b)
    strand_a.chars.zip(strand_b.chars).count{|a, b| a != b}
  end

  def self.assert_comparable!(strand_a, strand_b)
    if strand_a.size != strand_b.size
      raise ArgumentError, "Strands must be of equal length!"
    end
  end
end

module BookKeeping
  VERSION = 3
end
