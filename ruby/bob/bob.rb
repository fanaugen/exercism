class Bob
  def self.hey(remark)
    if remark.strip.empty?
      "Fine. Be that way!"
    elsif shouting?(remark)
      "Whoa, chill out!"
    elsif remark.end_with? "?"
      "Sure."
    else
      "Whatever."
    end
  end

  private_class_method

  def self.shouting?(remark)
    remark =~ /[A-Z]/ && remark.upcase == remark
  end

end
