class School
  def initialize
    @students_db = Hash.new { Grade.new }
  end

  def add(name, grade)
    students_db[grade] <<= name
  end

  def students(grade)
    students_db[grade].students
  end

  def students_by_grade
    students_db.sort.collect do |number, grade|
      {
        grade:    number,
        students: grade.students
      }
    end
  end

  private
  attr_reader :students_db

  class Grade
    def initialize
      @names = []
    end

    def <<(name)
      names.push(name)
      self
    end

    def students
      names.sort
    end

    private
    attr_reader :names
  end
end

module BookKeeping; VERSION = 3; end
