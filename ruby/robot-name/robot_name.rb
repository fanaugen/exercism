class Robot

  attr_reader :name
  @@robot_names ||= []

  def initialize
    reset
  end

  def reset
    self.name = unique_random_name
  end

  private

  def unique_random_name
    begin
      name = ("A".."Z").to_a.sample(2).join + "#{rand(100..999)}"
    end until unique_name?(name)
    name
  end

  def name=(new_name)
    @name = new_name
    @@robot_names << new_name
  end

  def unique_name?(name)
    ! @@robot_names.include?(name)
  end
end

module BookKeeping
  VERSION = 2
end
