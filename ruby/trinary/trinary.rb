class Trinary
  def initialize(base3)
    @bits = sanitize(base3)
  end

  def to_decimal
    @bits.chars.inject(0) do |decimal, digit|
      decimal * 3 + digit.to_i
    end
  end

  private
  def sanitize(str)
    return "0" if str =~ /[^012]/
    str
  end
end

module BookKeeping; VERSION = 1; end
