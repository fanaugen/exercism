module Strain
  def keep(&condition)
    self.each_with_object(self.class.new) do |item, items_to_keep|
      items_to_keep << item if condition.call(item)
    end
  end

  def discard(&condition)
    keep {|x| !condition.call(x)}
  end
end

class Array
  include Strain
end
