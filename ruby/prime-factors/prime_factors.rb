class PrimeFactors
  def self.for(number)
    factors = []
    prime = 2

    while number > 1 do
      prime = next_factor(number, prime)
      begin
        factors.push prime
        number /= prime
      end while number % prime == 0
    end

    factors
  end

  def self.next_factor(number, start_at = 2)
    (start_at..number).find {|i| number % i == 0 }
  end

  private_class_method :next_factor
end
