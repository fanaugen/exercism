class Phrase
  PUNCTUATION = Regexp.new("[.:,;?!/^@$%&]")

  def initialize(text)
    @words = text.split(/[,\s]+/)
  end

  def word_count
    tally = Hash.new(0)
    @words.each {|word| tally[normalize(word)] += 1}
    tally
  end

  private

  def normalize(word)
    word.downcase!
    word.gsub!(PUNCTUATION, "")
    word.sub(/^(['"])(.+)\1$/, "\\2")
  end
end

module BookKeeping
  VERSION = 1
end
