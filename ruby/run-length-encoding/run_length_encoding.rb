class RunLengthEncoding
  def self.encode(str)
    str.scan(/((.)\2*)/).map do |run, char|
      "#{length(run)}#{char}"
    end.join
  end

  def self.decode(rl_encoded)
    rl_encoded.scan(/(\d*)(\D)/).map do |num, char|
      char * multiplier(num)
    end.join
  end

  def self.length(run)
    run.length > 1 ? run.length.to_s : ""
  end

  def self.multiplier(str)
    return 1 if str.empty?
    Integer(str)
  end
  private_class_method :length, :multiplier
end

module BookKeeping; VERSION = 2; end
