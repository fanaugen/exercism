class SumOfMultiples
  def initialize(*factors)
    @factors = factors
  end

  def to(max)
    (0...max).reduce(0) do |sum, i|
      sum += i if @factors.any? {|f| i % f == 0}
      sum
    end
  end
end
