class Squares
  def initialize(upper_bound)
    @numbers = 0..upper_bound
  end

  def square_of_sum
    sum ** 2
  end

  def sum_of_squares
    squares.sum
  end

  def difference
     square_of_sum - sum_of_squares
  end

  private

  def sum
    @numbers.sum
  end

  def squares
    @numbers.map {|n| n * n}
  end
end

module BookKeeping
  VERSION = 3
end
